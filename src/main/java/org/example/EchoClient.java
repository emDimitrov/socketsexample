package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class EchoClient {
    public static void main(String[] args) {
        try {
            System.out.println("Client started");
            Socket soc = new Socket("localhost", 9806);

            System.out.println("Enter a string");
            BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
            String input = userInput.readLine();

            //Send information to socket
            PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
            out.println(input);

            //Read from socket
            BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
            System.out.println(in.readLine());

            userInput.close();
            out.close();
            in.close();

        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}